from helpers import get_config_parser, get_database, MyJSONEncoder, Product
from flask import Flask, g, jsonify

config = get_config_parser("config.ini")
database = get_database(config)

database.bind([Product])

app = Flask(__name__)

# Make jsonify() output dates as "%Y-%m-%d %H:%M:%S"
app.json_encoder = MyJSONEncoder


@app.route("/")
def index():
    return "Hello"


@app.route("/get_products")
def get_products():
    products = list(Product.select().dicts())
    return jsonify(products)


@app.before_request
def before_request():
    g.db = database
    g.db.connect()


@app.after_request
def after_request(response):
    g.db.close()
    header = response.headers
    header["Access-Control-Allow-Origin"] = "*"  # Enable CORS
    return response


if __name__ == "__main__":
    app.run(debug=True)
