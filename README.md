# 🌱 Vegan tooted 🌱

Scraping vegan products from [@ohseevegan](https://www.instagram.com/ohseevegan/) and displaying them in a table at https://andri.io/vegantooted/.

Using Python and [Instaloader](https://instaloader.github.io/) to load Instagram posts, [peewee ORM](http://peewee-orm.com/) and PostgreSQL to store them, [Flask](https://flask.palletsprojects.com/) to serve them and [Vue.js](https://vuejs.org/) with [vue-good-table](https://xaksis.github.io/vue-good-table/) to display them. Dark mode by [darkreader](https://github.com/darkreader/darkreader).

## Setup

### Backend

1. Make a venv: `python3 -m venv venv`, `source venv/bin/activate`

2. Download dependencies: `pip3 install -r requirements.txt`

3. Rename/copy the `rename_to_config.ini` file to `config.ini` and fill it with database and instagram info. There is an option to save Instaloader post objects in a [pickle](https://docs.python.org/3/library/pickle.html) file as well. It might work if you leave the instagram credentials empty, but it will probably redirect you to a login page and not let you download posts.

4. Run `scraper.py` to download Instagram posts from [@ohseevegan](https://www.instagram.com/ohseevegan/), parse them and save them to the database specified in `config.ini`.

    - If logging in doesn't work (`403 - Forbidden` or `400 - Bad Request`), you can [get cookies from Firefox](https://instaloader.github.io/troubleshooting.html#login-error). Save the session file into `data/<username>_session.bin`. 

5. Run `app.py` to launch a Flask server that returns the data in JSON format at `http://127.0.0.1:5000/get_products`. Deploy with `uWSGI` and `nginx` ([guide](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-20-04)).

### Frontend

1. Download dependencies: `npm install`

2. Run locally: `npm run serve`

3. Build files for production: `npm run build`

4. Deploy the `dist/` directory ([guide](https://cli.vuejs.org/guide/deployment.html#general-guidelines)) .
