from helpers import OhseeveganScraper
import helpers


def main() -> None:
    # TODO: Get parameters from command line arguments, not config file
    config = helpers.get_config_parser("config.ini")
    database = helpers.get_database(config)

    verbose = config["scraper"].getboolean("verbose")

    ohseevegan_scraper = OhseeveganScraper(verbose=verbose)
    ohseevegan_scraper.login(**config["instagram"])

    if config["scraper"].getboolean("read_posts_from_pickle"):
        ohseevegan_scraper.read_posts_from_file(config["scraper"]["read_file"])
    else:
        ohseevegan_scraper.download_posts(limit=config["scraper"].getint("limit_posts"))

    if config["scraper"].getboolean("save_posts_to_pickle") \
            and not config["scraper"].getboolean("read_posts_from_pickle"):
        ohseevegan_scraper.save_posts_to_file(
            config["scraper"]["save_file"],
            add_timestamp=config["scraper"].getboolean("add_timestamp_to_filename")
        )

    ohseevegan_scraper.convert_posts_to_products()
    ohseevegan_scraper.save_products_to_database(database)


if __name__ == '__main__':
    main()
