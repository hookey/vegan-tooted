import pickle
import re
import time
from typing import List, Tuple, Optional

import instaloader
import peewee

from helpers.models import Product


LABEL_MEANINGS = {
    "V🌱": "vegan",
    "GV🥐": "gluten_free",
    "SV🍭": "sugar_free",
    "PÕV🦧": "palm_oil_free"
}


class InstagramFeedScraper:
    def __init__(self, feed_name: str, verbose=False):
        self.feed_name = feed_name
        self.instagram_posts = []
        self.verbose = verbose
        self.api = instaloader.Instaloader(quiet=True)

    def login(self, username="", password="") -> None:
        session_filename = f"data/{username}_session.bin"

        # Use session file if available
        try:
            self.api.load_session_from_file(username, session_filename)
            test_login_output = self.api.test_login()
            if self.api.test_login() != username:
                if self.verbose:
                    print(f"Session file exists, but test login did not work. ({test_login_output} != {username})")
            else:
                return
        except FileNotFoundError:
            if self.verbose:
                print(f"Session file {session_filename} not found.")

        # Login and create new session file
        if username and password:
            if self.verbose:
                print(f"Logging in as {username}.")
                print(f"Saving session to {session_filename}")
            self.api.login(username, password)
            self.api.save_session_to_file(session_filename)
        elif self.verbose:
            print("Username and password not provided. Not logging in.")

    def download_posts(self, limit=0) -> None:
        self.instagram_posts = []

        profile = instaloader.Profile.from_username(self.api.context, self.feed_name)
        post_amount = limit or profile.mediacount  # get all posts if limit=0

        if self.verbose:
            print(f"Downloading {post_amount} posts from {self.feed_name}.")

        posts = profile.get_posts()
        counter = 0

        for post in posts:
            counter += 1
            self.instagram_posts.append(post)
            if self.verbose:
                print(f"{counter}/{post_amount}")
            if counter >= post_amount:
                break

    def save_posts_to_file(self, filename: str, add_timestamp=True) -> str:
        if add_timestamp:
            filename = OhseeveganScraper._add_timestamp_to_filename(filename)

        with open(filename, "wb") as f:
            pickle.dump(self.instagram_posts, f, pickle.HIGHEST_PROTOCOL)

        if self.verbose:
            print(f"Posts saved to {filename}.")

        return filename

    @staticmethod
    def _add_timestamp_to_filename(filename: str) -> str:
        # Add timestamp to filename before the first extension
        timestamp = str(int(time.time()))
        extension_index = filename.find(".")
        if extension_index != -1:
            filename = filename[:extension_index] + timestamp + filename[extension_index:]
        else:
            # If there is no extension, just add it to the end
            filename = filename + timestamp

        return filename

    def read_posts_from_file(self, filename: str) -> None:
        if self.verbose:
            print(f"Reading posts from {filename}.")
        with open(filename, "rb") as f:
            self.instagram_posts = pickle.load(f)


class OhseeveganScraper(InstagramFeedScraper):
    def __init__(self, verbose=False):
        super().__init__("ohseevegan", verbose=verbose)
        self.products: List[Product] = []

    def convert_posts_to_products(self) -> None:
        self.products.clear()

        for post in self.instagram_posts:
            try:
                product = OhseeveganScraper._product_from_instagram_post(post)
            except WrongFormatException as exception:
                if self.verbose:
                    print()
                    print(exception)
                    print(post.shortcode, post.caption[:50] + "...")
                    print()
                continue

            self.products.append(product)

        if self.verbose:
            print(f"{len(self.instagram_posts) - len(self.products)} posts were probably not about products.")
            print(f"Converted {len(self.products)} posts to products.")

    def save_products_to_database(self, database: peewee.Database) -> None:
        database.bind([Product])
        database.create_tables([Product])

        updated, saved = 0, 0

        for product in self.products:
            existing_product = Product.get_or_none(Product.shortcode == product.shortcode)

            if not existing_product:
                saved += 1
            else:
                updated += 1
                existing_product.delete_instance()

            product.save(force_insert=True)

        if self.verbose:
            print(f"Saved {saved} products to database. Updated {updated} products.")

    @staticmethod
    def _product_from_instagram_post(post: instaloader.Post) -> Product:
        details = OhseeveganScraper._details_from_caption(post.caption)

        product = Product(
            shortcode=post.shortcode,
            **details,
            likes=post.likes,
            comments=post.comments,
            timestamp=post.date_local,
            full_caption=post.caption
        )
        return product

    @staticmethod
    def _details_from_caption(caption: str) -> dict:
        # Preparing caption for anomalies
        caption = OhseeveganScraper._remove_caption_anomalies(caption)

        expression = (r"(?P<name>[^()\n]{1,80}?)\s*"  # name (lazy), 
                      r"(?:\((?P<brand>.+)\))?\s*"  # brand in parentheses, optional
                      r"(?:\s+~?\s*(?P<price>[\d\,.]+) ?€?)?\s*"  # 3,14€ or 3€ or 3.14€ or ~3,14 € etc, optional
                      r"\n(?P<labels>.+)\n\n"  # V🌱, GV🥐, SV🍭
                      r"(?P<description>(?:.|\n)+)\n+"  # Hea mõnes koogis kasutada ...
                      r"(?P<tags>#[\w+# \n]+)\s*")  # #taimetoit #vegantallinn #alpro #gluteenivaba #laktoosivaba ...
        match = re.match(expression, caption)

        if not match:
            raise WrongFormatException("Caption is probably not a product.")

        details = {
            "name": match.group("name").strip(),
            "brand": (match.group("brand") or "").replace("tootja ", "", 1),
            "price": 0,
            "description": match.group("description").strip(),
            "location": "",
            "vegan": False,
            "gluten_free": False,
            "sugar_free": False,
            "palm_oil_free": False,
            # "tags": match.group("tags")
        }

        # Determine labels based on second row of caption
        for label, meaning in LABEL_MEANINGS.items():
            if label in match.group("labels"):
                details[meaning] = True

        # If price was found in the caption, turn it into cents
        price = match.group("price")
        if price:
            details["price"] = int(float(match.group("price").replace(",", ".")) * 100)

        description, location = OhseeveganScraper._description_and_location_from_description(match.group("description"))
        details["description"] = description
        details["location"] = location

        return details

    @staticmethod
    def _remove_caption_anomalies(caption: str) -> str:
        # Remove last 2 lines when there is an accidental hashtag in the last line (fixes CFR8l78JmlV description)
        caption_lines = caption.splitlines()
        if caption_lines[-1] == "#taimetoit":
            caption = "\n".join(caption_lines[:-2])

        # Remove slice price of maxima pizza (CFwdLJepV3s)
        if "~1,80€ tükk" in caption_lines[0]:  # NB! strange ü
            caption = caption.replace("~1,80€ tükk ja ", "").replace(" terve pitsa", "")

        # Fix price of Robebox (CKcANU9Jp8g)
        if "~21,90€ - 24,00€" in caption_lines[0]:
            caption = caption.replace("~21,90€ - 24,00€", "24,90€")

        # Move trailing quantity of burger patties to title without parentheses (CEqyCKUJBNu)
        if caption_lines[0] == "VEGAN BURGERPIHV (Moving Mountains) 21.90€ (8tk)":
            caption = caption.replace(" (8tk)", "", 1).replace("PIHV", "PIHV 8tk", 1)

        # CQE6eUErSRC
        caption = caption.replace("𝐏𝐢𝐬𝐢𝐤𝐞 𝐦𝐞𝐞𝐥𝐝𝐞𝐭𝐮𝐥𝐞𝐭𝐮𝐬 𝐞𝐧𝐧𝐞 𝐣𝐚𝐚𝐧𝐞🔥\n", "")

        return caption

    @staticmethod
    def _description_and_location_from_description(description: str) -> Tuple[str, str]:
        description_lines = description.strip().splitlines()
        description_last_line = description_lines[-1]

        location = ""

        # If the last line of the description ends with a shopping cart, that line is the location of the product
        # Also checking the first word in case the shopping cart isn't there
        # TODO: Some have locations as multiple lines (CG3Pz2dJrhI, CG4pNnRp1uz, ...), could catch with regex...
        if description_last_line[-1] == "🛒" \
                or description_last_line.split()[0].lower().strip(":") in ["leiad", "leiab", "nähtud", "saadaval"]:
            location = description_last_line.strip()
            description = "\n".join(description_lines[:-1]).strip()

        return description, location


class WrongFormatException(Exception):
    pass
